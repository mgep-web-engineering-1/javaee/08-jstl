<%@ page language="java" contentType="text/html; charset=UTF-8"
pageEncoding="UTF-8"%> <%@ taglib prefix="c"
uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1" />
    <link rel="stylesheet" href="css/style.css" />
    <title>Testing JSTL - Scopes</title>
  </head>
  <body>
    <article>
      <p>We didn't set any variable in this page.</p>
      <table>
        <tr>
          <td><b>Default Level</b></td>
          <td><c:out value="${test}" /></td>
          <td>
            <p>
              It can be seen that if we do not set anything, the default value
              is different.
            </p>
            <p>
              In this case, as there is not page or request level variables with
              named 'test', session variable is printed.
            </p>
            <p>Its logic is:</p>
            <ul>
              <li>print <b>pageScope.test</b></li>
              <li>if null => print <b>requestScope.test</b></li>
              <li>if null => print <b>sessionScope.test</b></li>
              <li>if null => print <b>applicationScope.test</b></li>
            </ul>
          </td>
        </tr>

        <tr>
          <td><b>Page Level</b></td>
          <td><c:out value="${pageScope.test}" /></td>
          <td>Page scope variable is lost.</td>
        </tr>

        <tr>
          <td><b>Request Level</b></td>
          <td><c:out value="${requestScope.test}" /></td>
          <td>
            <p>Request scope variable is lost.</p>
            <p>
              This is different from url request variables, so even if we add
              <i>?test=example</i> in the URL, nothing will show.
            </p>
          </td>
        </tr>

        <tr>
          <td><b>Session Level</b></td>
          <td><c:out value="${sessionScope.test}" /></td>
          <td>
            Session scope variable continues existing because it was set in
            index.jsp.
          </td>
        </tr>

        <tr>
          <td><b>Application Level</b></td>
          <td><c:out value="${applicationScope.test}" /></td>
          <td>
            Application scope variable continues existing because it was set in
            index.jsp.
          </td>
        </tr>
      </table>
      <p>
        Just in case, I would recommend using always the scope, and avoiding the
        default option.
      </p>
      <p>Go back to <a href="/#scopes">index#scopes</a>.</p>
    </article>
  </body>
</html>
