<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri = "http://java.sun.com/jsp/jstl/fmt" prefix = "fmt" %>
<fmt:requestEncoding value='UTF-8'/>

<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8" />
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="css/style.css" />
  <title>Testing JSTL</title>
</head>

<body>
  <h1>Testing JSTL</h1>
  <section>
    <h2>1. Playing with variables</h2>
    <div class="grid-container">
      <article>
        <h3>1.1 Setting and printing</h3>
        <p>
          We will set the variable "username" in the session using JSTL (see JSP file at this point)
          <c:set var="username" scope="session" value="${'Alain'}" />
        </p>
        <p>
          Hi
          <c:out value="${sessionScope.username}" default="Unknown User" />.
          <i>Usually, setting session variables is done in the controller.</i>
        </p>
      </article>

      <article>
        <h3>1.2 Escaping tags</h3>
        <p>In regular JSP/HTML, tags cannot be escaped and we have have to use &amp;lg; and this kind of tricks to print
          html
          tags.</p>
        <p>Example: <i>&amp;lt;exampletag/&amp;gt; to print &lt;exampletag/&gt;</i></p>
        <p>With JSTL, we can use
          <c:out value="${'<c:out />'}" /> to print scape this kind of characters (see the code in JSP):</p>
        <p>Example: <i>
            <c:out value="${'Text output using <c:out/>, you can escape tags such as <exampletag/>'}" /></i></p>
      </article>
      <article>
        <h3>1.3 Printing parameters (GET and POST)</h3>
        <p> You can also write variables from the request:
          <b>
            <c:out value="${param.myname}" default="Use one of the forms below" />
          </b>
        </p>
        <form action="index.jsp" method="get">
          <input type="text" name="myname" placeholder="Your Name (GET)" />
          <input type="submit" value="Send using GET" />
        </form>
        <form action="index.jsp" method="post">
          <input type="text" name="myname" placeholder="Your Name (POST)" />
          <input type="submit" value="Send using POST" />
        </form>
      </article>

      <article>
        <h3>1.4 Remove variables</h3>
        <c:remove var="username" />
        <p>Hi
          <c:out value="${sessionScope.username}" default="Unknown User" />
        </p>
      </article>
    </div>
  </section>

  <h2>2. Iterating (Foreach)</h2>
  <p>We can do a "for" <b>c:forEach</b> and <b>c:out</b> (see code in JSP files).</p>
  <section>
    <div class="grid-container">
      <article>
        <h3>2.1 Single dimension</h3>
        <p>We can do a "for" from 1 to 5:</p>
        <ul>
          <c:forEach var="i" begin="1" end="5">
            <li>Item
              <c:out value="${i}" />
            </li>
          </c:forEach>
        </ul>
      </article>
      <article>
        <h3>2.2 Multiple dimensions</h3>
        <p>We can do a "for" from 1 to 10 (for rows) and from 1 to 20 (for columns):</p>
        <table>
          <c:forEach var="i" begin="1" end="10">
            <tr>
              <c:forEach var="j" begin="1" end="20">
                <td>
                  <c:out value="${i}-${j}" />
                </td>
              </c:forEach>
            </tr>
          </c:forEach>
        </table>
      </article>
      <article>
        <h3>2.3 Iterating a list</h3>
        <p>If we have a Java array, ArrayList, List..., we can do a slightly different foreach:</p>
        <%
          // Again, this is usually done in the Controller
          String[] loggedUsers = {"Alain","Enaitz","Dani","Goiuria","Xabier"};
          session.setAttribute("loggedUsers",loggedUsers);
        %>
        <ul>
          <c:forEach items="${sessionScope.loggedUsers}" var="loggedUser">
            <li>User:
              <c:out value="${loggedUser}" />
          </c:forEach>
        </ul>
      </div>
      </article>
  </section>

  <h2 id="scopes">3. Scopes</h2>
  <article>
    <p>We will create variables in different scopes with the same name: 'test'.</p>
    <p>See the JSP code so you can understand it better:</p>
    <ul>
      <li>
        <p>Page level values are set only for the current page.</p>
        <c:set var="test" value="Page Level Value" scope="page" />
      </li>
      <li>
        <p>Request Level values are lost when a new request is made.</p>
        <c:set var="test" value="Request Level Value" scope="request" />
      </li>
      <li>
        <p>Session level values are lost when a session is closed. It is used to store a value for a specific user as long as
          they continue logged. Usually Session is closed when the user logs out or the browser expires the cookie.</p>
        <c:set var="test" value="Session Level Value" scope="session" />
      </li>
      <li>
        <p>Application level values are used to share a value among all the users of the application.</p>
        <c:set var="test" value="Application Level Value" scope="application" />
      </li>
    </ul>
  
    <table>
      <tr>
        <td><b>Default Level</b></td>
        <td>
          <c:out value="${test}" />
        </td>
      </tr>
  
      <tr>
        <td><b>Page Level</b></td>
        <td>
          <c:out value="${pageScope.test}" />
        </td>
      </tr>
  
      <tr>
        <td><b>Request Level</b></td>
        <td>
          <c:out value="${requestScope.test}" />
        </td>
      </tr>
  
      <tr>
        <td><b>Session Level</b></td>
        <td>
          <c:out value="${sessionScope.test}" />
        </td>
      </tr>
  
      <tr>
        <td><b>Application Level</b></td>
        <td>
          <c:out value="${applicationScope.test}" />
        </td>
      </tr>
    </table>
    <p>Go to another page: <a href="scopes.jsp">scopes.jsp</a></p>
  </article>

</body>

</html>