# Retrieve Parameters in JSP

## Make it work

* Clone the repository.
* Open folder with VSCode.
* Right click and Run `src > main > java > ... > Main.java`.
* Open http://localhost:8080 in the browser.

## Objective

It is quite confusing to have tags and java code mixed in the same files (JSP). Therefore, there are some libraries to **completely** remove the Java code from the JSP files.

We will be using **JSTL library** at class (made by the creators of JSP themself). There are other similar libraries, some frameworks have their own libraries with this exact purpose.

## Warning

If you are not seing right the HTML, it may be because you have an old CSS on the cache. Press ```CTRL+F5``` so the CSS is reloaded.

## Explaination

You need to add JSTL library as a dependency in ```pom.xml```:

```xml
...
<dependency>
    <groupId>org.glassfish.web</groupId>
    <artifactId>jakarta.servlet.jsp.jstl</artifactId>
    <version>2.0.0</version>
</dependency>
...
```

Then, if you want to use in a JSP file, you have to "import" the taglib, and you will be able to use those prefixes (```c``` or ```fmt``` in this case):

```jsp
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri = "http://java.sun.com/jsp/jstl/fmt" prefix = "fmt" %>
...
```

That way we can use those taglibs for different scenarios.

* To set the charset for the request:

```jsp
...
<fmt:requestEncoding value='UTF-8'/>
...
```

* To print a value that is on session:

```jsp
...
<c:out value="${sessionScope.username}" default="Unknown User" />
...
```

* To iterate an array:

```jsp
...
<ul>
    <c:forEach var="i" begin="1" end="5">
    <li>Item
        <c:out value="${i}" />
    </li>
    </c:forEach>
</ul>
...
```

* Set/remove a variable
  * Most of the times is done in the controllers, but you can change them in JSP too.
  * More about scopes in the aditional explainations.

```jsp
...
<c:set var="username" scope="session" value="${'Alain'}" />
...
<c:remove var="username" />
...
```

Aditional explainations are written in the web pages.
Execute the server and check what is written and how is shown.

## Next and before

* Before [07-jsp-templates](https://gitlab.com/mgep-web-engineering-1/javaee/07-jsp-templates)
* Next [09-internationalization](https://gitlab.com/mgep-web-engineering-1/javaee/09-internationalization)
